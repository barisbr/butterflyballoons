module.exports = {
  component: 'fa',
  imports: [
    //import whole set
    {
      set: '@fortawesome/free-solid-svg-icons',
      icons: ['fas']
    },
    {
      set: '@fortawesome/free-brands-svg-icons',
      icons: ['fab']
    }
  ]
}
