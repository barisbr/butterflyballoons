const mongoose = require('mongoose');

function randomString(length) {
  const chars = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

let schema = new mongoose.Schema({
  TID: {
    type: String,
    required: true,
    index: true
  },
  GID: {
    type: String,
    required: true,
    index: true
  },
  Date: {
    type: String,
    required: true,
    index: true
  },
  DateTime: {
    type: Number,
    required: true,
  },
  Transactions: [
    {
      Name: {
        type: String,
        required: true,
      },
      Description: {
        type: String,
        required: true,
      },
      Date: {
        type: Date,
        required: false,
        default: new Date().getTime()
      }
    }
  ],
  CreatedAt: {
    type: Date,
    required: false,
    default: new Date().getTime()
  },
  CancelAt: {
    type: String,
    required: false,
    default: null
  },
  CancelWhy: {
    type: String,
    required: false,
    default: ''
  },
  UpdatedAt: {
    type: Date,
    required: false
  },
  BalloonID: {
    type: String,
    required: false,
    default: ''
  },
  Compartment: {
    type: String,
    required: false,
    default: ''
  },
  ServiceID: {
    type: String,
    required: false,
    default: ''
  },
  ServiceTime: {
    type: String,
    required: false,
    default: ''
  },
  PICKUP: {
    type: Boolean,
    required: false,
    default: false
  },
  Messages: {
    type: Array,
    required: false,
    default: []
  }
}, {strict: false})

const model = mongoose.model('tickets', schema)
exports.Ticket = model

exports.TID = () => {

  return new Promise((resolve, reject) => {

    const key = randomString(7);

    model.findOne({TID: key}, {_id:1}).then(isFound => {
      if (isFound === null) {
        return resolve(key)
      }

      return this.random()
    })

  })

}
