const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  ID: {
    required: true,
    type: String,
    unique: true
  }
}, {strict:false})

const Order = mongoose.model('orders', schema)

exports.Order = Order;
