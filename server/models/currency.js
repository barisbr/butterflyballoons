const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  Date: {
    type: String,
    required: true,
    unique: true,
    index: true
  },
  Base: {
    type: String,
    required: true
  },
  Rates: {
    EUR: {
      type: Number,
      required: false,
    },
    CAD: {
      type: Number,
      required: false,
    },
    HKD: {
      type: Number,
      required: false,
    },
    ISK: {
      type: Number,
      required: false,
    },
    PHP: {
      type: Number,
      required: false,
    },
    DKK: {
      type: Number,
      required: false,
    },
    HUF: {
      type: Number,
      required: false,
    },
    CZK: {
      type: Number,
      required: false,
    },
    AUD: {
      type: Number,
      required: false,
    },
    RON: {
      type: Number,
      required: false,
    },
    SEK: {
      type: Number,
      required: false,
    },
    IDR: {
      type: Number,
      required: false,
    },
    INR: {
      type: Number,
      required: false,
    },
    BRL: {
      type: Number,
      required: false,
    },
    RUB: {
      type: Number,
      required: false,
    },
    HRK: {
      type: Number,
      required: false,
    },
    JPY: {
      type: Number,
      required: false,
    },
    THB: {
      type: Number,
      required: false,
    },
    CHF: {
      type: Number,
      required: false,
    },
    SGD: {
      type: Number,
      required: false,
    },
    PLN: {
      type: Number,
      required: false,
    },
    BGN: {
      type: Number,
      required: false,
    },
    TRY: {
      type: Number,
      required: false,
    },
    CNY: {
      type: Number,
      required: false,
    },
    NOK: {
      type: Number,
      required: false,
    },
    NZD: {
      type: Number,
      required: false,
    },
    ZAR: {
      type: Number,
      required: false,
    },
    USD: {
      type: Number,
      required: false,
    },
    MXN: {
      type: Number,
      required: false,
    },
    ILS: {
      type: Number,
      required: false,
    },
    GBP: {
      type: Number,
      required: false,
    },
    KRW: {
      type: Number,
      required: false,
    },
    MYR: {
      type: Number,
      required: false,
    }
  }
})

const model = mongoose.model('currencies', schema)

exports.Currency = model
