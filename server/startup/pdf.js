const puppeteer = require('puppeteer')
const moment = require('moment')
var browser = null;

const headerTemplate = () => {
  const date = moment().format('DD-MM-YYYY')
  return `
<style type="text/css">
		#header-template {
			margin: 15px 27px 0px 28px;
			padding: 0;
			font-size: 10px !important;
			color: #808080;
			background-color: whitesmoke;
			border: solid 1px #9c9c9c;
			-webkit-print-color-adjust: exact;
			position: relative;
			width: 100%;
			height: 50px;
			box-sizing: border-box;
			position: relative;
		}
		
		.logo {
			background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAAAyCAMAAABGUCSuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZBRURGNTc5RjgwMzExRTlCNzFDRjg4NjQ5MzkwRkY4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZBRURGNTdBRjgwMzExRTlCNzFDRjg4NjQ5MzkwRkY4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RkFFREY1NzdGODAzMTFFOUI3MUNGODg2NDkzOTBGRjgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkFFREY1NzhGODAzMTFFOUI3MUNGODg2NDkzOTBGRjgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5ADodQAAADAFBMVEX+6+qu3mfI2/CasZ75mZXy9vv81dP5iYX0JB7/+PjF55P////3lRJ1nNXJxyj6/fa34Xj0MisqaMD2S0XF1+6MxWjOvCf0KCG1zOn+9vX0+Pzs8fn//Pz79wT3WBn5iBPb5vT0JyD2Qzz8zMqGumtKh5AMUbb52An3Sxs3bcH2UUqcuuL8/vqTzF8ASLLp9tVqltMqap3+8vH3XViSst77wb7U4vL5/P36tbHS7an4Nx2EpFltmNPl7fdbl4S+0+1JfMj95uQ8csT3WlX8xgpopXr4r6r1OjVVhc1Ed4n85wb3Ylz5eBWk2VVmktD5Zxb5fnre6PV4l2P4aWUVVrgzapb7r64bW7r9/v6LrdwARrL3cWygveP4kY3j88rp8Pjg6/b7xMD5gn6p3lP95OP7uLXslRj0NTCXu+P7sq8rZb6Ut+A7eJdVg32kweSsxeb3+fwGTrRBdcW4sDZNgcrjyBgRU7f5qKX4enYiYbzImi/3eXOCpdhcis76oJv6pw/0LSbx+eULTrW/5IcoYqMnZr/4Z2L3ZWDn7vj7urj0Lynjpxvv8/r4dnSi2VIhXrv7yMX6paF8oNbN6qH2Pjh+o9j93twDS7T4a2j4c3D0+ulgjs8mYb31bWS6z+r92dff8cNIescDSrOm21n94+H4bmiGqNn96egFS7Ta77n+8O9rkW794N71LiCl2lX7wLwjZr///f3O3fD9/vsDSLIUV6sFSbMWWLmYtuBhi3X8/f5xoNcwcML8vbsAS7Nbj9Dn8Pj+9PP4/PEfYLz7vbqn21T3nRF1sXLjKCvuOSfu+OB9oF/1PB6m3FODkFvn7/j3X1mUr00uacD58vT49PbX3R7aZDM6bZI+dY5FeMdHf8kZSKkQSrDzKSSwyej1KiP7tQ3i1uH6rKn80dCj2VNjjs+qzFGe1FmyyOe6PFT4fXjj7PfqthbQzdZUTZNEX6/2VU/4VVRSfIAZWLl0r3LGLj99uG2g11et4VH74eAwaMD5Lyj2MRz0KSIASrP///93r+mYAAABAHRSTlP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AU/cHJQAACJVJREFUeNrMmAtYVMcVgIEAu+uKgmAu4MJyF4sLyFVAAeOaCEhBAwJByQqsKCAbDSoSRElMxAesWVFQUIlaVHyQEB9dtJVsK1ZN6CNakTYlfXxsK9qk2IeNbdOmzJ2cmbu8sotsvvhB7gc7d+bOmf+eM+ecmTt2eCwvu1EjGewkY0eP3qPQrbo2RvQwvXzBVt/agxgbx4C+KdnnhYV32pizkqkOo0/PaZv7bMizS3frw/72J5ztNMp0kXxtyPkP/rMwMVOkq37qqVGmOyvnxp1Pyi2YwP7mz9/71a9HmS6J373zk/Pn4+74nvu5W+w/90TZjWrE+fe+snPq5wWvKMcrx+/ene9b5ijrp0/ftu3wRXIT6ecX4f1EcGryD5dQ84p+7ebT73XOVU7zndAZWhD63kblHqlAnzjH/t7eJPua61POvNiQwZfMfPANycb6Ix1VXdi79MaNxZSt0fc+Y2ITF8jHT/MNDcnNvfXTzkU3qyn9uv29JHKdnHyUF64Sv2+mtioDBnkJV27g+flQl1b4bnzhs1/m97I3fRL/GpoLrNyCj+TthD7dVYAnff4DM5xvXWJp/YisrCwSp/OCs4KzH09/juOH0PPO+XT+KyQudFrZHxeZpr37nU9u3Zq60ydZQ+jL9xK9Xe2T4ohQSUwAEQ22GPJHHHcpFcosjst47rFw6YlWnl9T5NFPbxIv6pyc+9+CCUqfRDHaeufj0NC1H8m1EkIfB3D7w3j6nFCg3nfC2YFQFlnSeb6B0NdxPPd4OoG6GM03hI41yo1AfHf81gW+CxLFzyTL5cm9sY3U512BfpxMPxg+I9VsuJJIa3Q/C7rRaLSg+4F8Fh5M99aIk/Pb5DcXKZ9e+vGr53Qi0WpnmRDvO4C+nPjKZZ4/dJvMMEivgUjZn5DgQgJmCy2Bfmh+gorQ+fkJ9wk00vN+6eWqhHVC2oZuwdhv8YlsB7BdYEJCs5ke+XpCQkv03ZSUvyQnyj+bHLLzVa3XQK47Dk7nOhHUWCLQvY+A9Bl4NonnO0ifIp7/PqXDVUrpPB8AtnEqorc89yiCdPsxz19tvsQ3rHAQmvu8zgl+adz9o/ffyg/jPpi81nflAP2iPZn4HTWUPk/1uxeJ7xWOTPf+oj9Efhgh0BMgZC3p28300t//4Wf5S+OmLmzLHJTnD58k8WZP6Ss6iGCDB7ZKLwkIpPSAkkeReD9RO6b4MhF4VEjp4OvcIUrfUBLgaUHnW//+//wPwd1TBujXhXgfTOerblujcyu8IyHieM7BuxBXXuL57c1q7NcA/WMqKZ3nXJojjIT+cqG31JLOd/xPoWyTM3n99Is7CNx1xzhK3+Uyv+htGGZNoRX6kIirh7ILY08YmztTKFiePyCFG0of8PnB9HJ8LT1ns/PA3mbb3ntJJ3+BB7wOzwNjch4j0V2gXmncAqQl9djsdRk0Qz+W/pVd5RzQfBzuo/8ECwPzLiPRITI2ODlAQi/ahfvoDQ++Ln2cdXqxDbpzD6T7M8Dqy4pntkRS+q6vS68hHjdxsOULY4A+k9JLIdtIJ/V73S6BnkFCwhMYB3Ah3E6B4b9QD08PoLpgY5UV+gzqdO8cX07p2V0vLysizltPdea61Or6S9QGL5EcFBFBqa9HNEsrwTm5WU7YGAFxz63Hw9LV5TCug1r9KSe8xRC62l5YX4dEHL8dllga0OWTSO5QwZhkzW44ileQOgfZZhndCXSUQr31qHF4Oj5ABMvLSfd1Ft8yM6zQM9YT38/oS2Zk8TFWtdJcp/6tOddFxvQ9FnYjw9Kz3x5u20By3XK6txlE50qySNzi9dvNhqArVuoSGPUGjHmUE/J85WJzng9MxcPST0DpEWBeD/ysfcdNn/FOTc0c6RaVSuXdolK1dE0xP82eVdzRUTxrnlBz8mxRzaJlsIoufupml6KOGJWHsBoHgzhd7VJVqpnNNIpcVKpPyc3tlkOQFtY7PfFvWLVNvcg6Fzhm3++VT5oukw6pGYY7NDDYSBfV1dW9JepbAhrr0oSHhsy6FEpyTkvz7zsLcIytdc85Zq55rdIpFKffJ3sl3J2WRlaw2WlpZKC8zQpFTp5NdHdELnGUUNMg5CiopWUUlJ7OMN8VnlXrGdKV1VDF7NxZWgtvgsoV1hQErbNZ5k14XxZBR32TbfSyIBgonuog0SPkJhHo6Cv0s6yJfZiTCcB9pKeCYeJTerQIxcoIHbHAfZPQDaCPYw+LRLbR42Wy06iMnq5cYE0mttoq3RCE2E0A6tYjFuTfYNBrYRiHQetsSkdlTfguAvq1MpM7lvT0OOLCI1evHhiJrq1eVYtOE5T0Idq8B7VLrdEPitHztCGKYe5iQy0qo3vURmim9DLUI00ndBiEEUXbeGIGdmJZpKXd88Rs9WxWHG2NvoplVgszIEZv4UYG6QT/iCdTdYUVv1/GOl+h835WD/Pe02gr3YRYpCCzrWHYJjuW+p0FfXUfPZrSWaIy6Vdrph+LYhX+YkLHjRrwT7dom+hMvJ1dOksQkjLk7uysQPCtZUl3FqM02rCSZd7AEi3SU+/MY1GsFOjsBVkQE8QQy0skkqZ9LKqwTXdw91NilEN8Dgnh5z+UTqNRpjCx6dDSqDWJQV5kYnIAH6ZD7AVM6cQoZN6d9W49RLzWYBNdn5apReDphoeIBCr86aREXF9RoesGOgpKq4DM0y1GrG6fyA0CHsSuaRET5KhRMMidRhyZFg0j+DwybdKwKNzGeadJQ0bevf1UXt6pIAR+B3REVUmnKUZhwNK7Jvpq4h6aGo7FC4JBXjTboHRySEQEMFidYagBR6Zvbm9vf/50FFhxU3s4lbgQ3g45Qwft7eHduDuc3GQSMx5MqdVrK/zNFg1zDNK7PVxN486fiJCCjGBIhwexK5/8GbUkbPCRs6zJy/rcysK8DN+GE/JvH/1LAQYAOFp5+Q/J0/gAAAAASUVORK5CYII=) no-repeat 50% 50%;
			background-size: contain;
			display: inline-block;
			width: 120px;
			height: 42px;
			position: absolute;
			left: 0;
			top: 0;
		}
		#page-number {
			position: absolute;
			right: 25px;
			top: 11px;
		}
		
		#date {
			position: absolute;
			right: 25px;
			top: 26px;
			color: #1d1d1d;
			font-weight: bold;
		}
		
		.label {
			font-size: 10px;
			color: dimgray;
		}
		
		span.content {
			color: #333 !important;
			font-weight: bold;
		}
	
	</style>
  <div id="header-template">
    <span class="logo"></span>
    <span id="page-number" class="label">Page : <span class="content"><span class="pageNumber"></span> / <span class="totalPages"></span></span></span>
    <span id="date">${date}</span>
  </div>
`;
}
const getContent = (content) => {
  return `
<style type="text/css">
		body, html {
			padding: 0;
			color: #333;
			font-size: 15px;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
		
		.ticket-item {
			border-radius: 4px;
			border: solid 1px #9c9c9c;
			background: #fefefe;
			padding: 10px;
			display: flex;
			align-items: center;
			justify-content: space-between;
			flex-wrap: wrap;
			margin: 10px 0;
		}
		
		.ticket-item > div {
			flex: 1;
			padding: 5px;
		}
		
		.ticket-item label {
			display: block;
			font-size: 13px;
			font-weight: bold;
			color: #6a7183;
			margin-bottom: 5px;
		}
		
		.ticket-item span {
			display: block;
			font-size: 15px;
			color: #535353;
			margin-bottom: 5px;
		}
	</style>
<div>${content}</div>`
}
const footerTemplate = () => {
  return `
  <style type="text/css">
  #footer-template {
    margin: 15px 27px 0px 28px;
    padding: 0 8px;
    font-size:9px !important;
    color:#696969;
    background: #f5f5f5;
    background: -moz-linear-gradient(top, #f5f5f5 0%, #eeeeee 100%);
    background: -webkit-linear-gradient(top, #f5f5f5 0%,#eeeeee 100%);
    background: linear-gradient(to bottom, #f5f5f5 0%,#eeeeee 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5f5', endColorstr='#eeeeee',GradientType=0 );
    border: solid 1px #9c9c9c;
    -webkit-print-color-adjust: exact;
    position: relative;
    width: 100%;
    height: 22px;
    box-sizing: border-box;
    position: relative;
    line-height: 19px;
    display: flex;
    justify-content: space-between;
  }
</style>
<div id="footer-template"><div>butterflyballoons.com</div><div style="text-align: right">fly@butterflyballoons.com</div></div>
`;
}

module.exports = {
  async setup() {
    browser = await puppeteer.launch();
  },
  async create(filename, content, landscape = false) {
    const page = await browser.newPage();
    await page.setContent(getContent(content));
    return await page.pdf({
      path: `static/tickets/${filename}`,
      landscape,
      format: 'A4',
      displayHeaderFooter: true,
      footerTemplate: footerTemplate(),
      headerTemplate: headerTemplate(),
      printBackground: true,
      margin: {
        top: '3.2cm',
        left: '1cm',
        right: '1cm',
        bottom: '3cm',
      }
    });
  },
}
