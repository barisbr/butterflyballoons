const mailgun = require("mailgun-js");
const config = require("config");
const path = require("path");
const fs = require("fs");

const htmlTemplate = (name, subject, messages) => {
  const paragraphs = []
  messages.forEach(item => {
    paragraphs.push(`<p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">${item}</p>`)
  })
  return `<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">
<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
	<tbody>
	<tr>
		<td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
			<table class="content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
				<tbody>
				<tr>
					<td class="header" style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;padding: 25px 0;text-align: center;/* background: #2680eb; */">
						<div style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;align-items: center;margin: auto;padding: 10px 0;color: #464646;font-size: 14px;font-weight: bold;">
							<a href="https://butterflybuttons.com" class="main" style="box-sizing: border-box;font-size: 19px;font-weight: bold;text-decoration: none;text-shadow: 0 1px 0 white;display: inline-flex;align-items: center;color: #464646;letter-spacing: 2px;font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;">
								<img alt="Butterfly Balloons" src="http://butterflyballoons.com/template/fly/images/logo.png" width="auto" height="60px" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; max-width: 100%; border: none; pointer-events: none;">
							
							</a>
						</div>
						<div style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;align-items: center;margin: auto;padding: 10px 0;color: #464646;font-size: 14px;font-weight: bold;">
							${subject}
						</div>
					</td>
				</tr>
				
				<!-- Email Body -->
				<tr>
					<td class="body" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
						<table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 700px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
							<!-- Body content -->
							<tbody>
							<tr>
								<td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
									<h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">Hello ${name},</h1>
									${paragraphs.join('\n')}
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				
				<tr>
					<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
						<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
							<tbody>
							<tr>
								<td class="footer-cell" align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px 0 20px 0; font-size: 14px">
									© 2020 <b style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; font-size: 14px">Butterfly Balloons</b><br>All rights reserved.
									<address class="address" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; font-size: 12px; margin-top: 5px;">
										Aydinli-Orta Mah. Uzundere Cad. No:29 50180 Goreme, Cappadocia, TR <b style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">+90 (384) 271-3010</b>
									</address>
								
								</td>
							</tr>
							
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>

</body>
</html>
`
}

exports.send = (subject, to, name, messages, attachment = false) => {

  return new Promise((resolve, reject) => {

    const mg = mailgun(config.get('mailgun'));
    const data = {
      from: 'Butterfly Balloons <noreply@butterflyballoons.com>',
      to: `${name} <${to}>`,
      bcc: `Yeni Online Rezervasyon (${name}) <fly@butterflyballoons.com>`,
      subject,
      html: htmlTemplate(name, subject, messages),
      'h:Reply-To': 'fly@butterflyballoons.com'
    };
    if (attachment) {
      data.attachment = new mg.Attachment({data: fs.readFileSync(path.join(__dirname, '../../static/tickets/' + attachment)), filename: 'reservation-details.pdf'});
    }
    mg.messages().send(data, function (error, body) {
      return resolve()
    });

  })

}
