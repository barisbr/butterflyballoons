const express = require('express')
const moment = require('moment')
const config = require('config')
const request = require('request')
const axios = require('axios')
const uniqid = require('uniqid')
const bodyParser = require('body-parser')
const uuidv4 = require('uuid/v4')
const CryptoJS = require("crypto-js")
const {forEach} = require('p-iteration')
const storeKey = '30313332322e747279326d6530313332322e747279326d65'
const bankPassword = '1322.k2K'
const bankTerminalID = '10202016'
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'))
const {Capacity} = require('../models/capacity')
const {Flight} = require('../models/flight')
const {Order} = require('../models/order')
const {Coupon} = require('../models/coupon')
const {Currency} = require('../models/currency')
const {Reservation, OnlineKey} = require('../models/reservation')
const HEX = require('../tools/hex')
const Mailer = require('../tools/mailer')
const PDF = require('../startup/pdf')
const IframeResponse = require('../tools/iframe-response')
const hashedPassword = HEX.convert(bankPassword + '0' + bankTerminalID)
const SHA = require('js-sha512');

const API = require('../tools/api');
const router = express.Router();

router.post('/coupon', async (req, res) => {
    var {token, code} = req.body;

    try {
        tokenData = JSON.parse(CryptoJS.AES.decrypt(token, config.get('aes_key')).toString(CryptoJS.enc.Utf8))
      } catch (error) {
      }
    
      if (!tokenData) return API.error(res, 'Your session have expired.')

      const check = await Coupon.findOne({Code: code, IsUsed: false});

      if (check) return API.success(res, 10)

      return API.error(res, 'Coupon code is invalid or redeemed before')
})

router.post('/capacity', async (req, res) => {

  const {date} = req.body;

  const {error} = Joi.object({
    date: Joi.date().format('YYYY-MM-DD').raw().required(),
  }).validate({date});

  if (error) return API.validation(res, error)

  if (moment(date, "YYYY-MM-DD").diff(moment().startOf('day'), 'days') < 1) return API.error(res, 'Cannot make reservation for this date')

  const dateDDMMYYYY = date.split('-').reverse().join('-')

  let flights = await Flight.find({IsDeleted: false}).lean()

  let currencies = await Currency.findOne({}, null, {sort: {_id: -1}}).lean()
  const {GBP, TRY, USD} = currencies.Rates

  let cp = await Capacity.findOne({Date: dateDDMMYYYY, Online: {$ne: false}})

  let capacities = []

  if (!cp || !currencies) {

    flights.forEach(flight => {
      if (!flight.IsDeleted) capacities.push({
        ID: flight._id.toString(),
        Name: flight.Name,
        Capacity: 0,
        Features: flight.Features,
        Price: {
          EUR: 0,
          USD: 0,
          GBP: 0,
          TRY: 0,
        }
      })
    })

    return API.success(res, {
      flights: capacities,
      currencies: {
        USD,
        GBP,
        TRY,
      },
      date: dateDDMMYYYY,
      token: null
    })

  }

  const reservations = await Reservation.find({ReservationDate: date.split('-').reverse().join('-'), CancelDate: null}).lean()

  capacities = []

  cp.Capacities.forEach(item => {

    const fl = flights.find(f => f._id.toString() === item.ID && f.IsDeleted === false)

    if (fl) capacities.push({
      ...item,
      Name: fl.Name || cp.Name,
      Capacity: cp.Online ? item.Capacity : 0,
      Features: fl.Features,
      Price: {
        EUR: item.Price,
        USD: parseFloat((item.Price * USD).toFixed(2)),
        GBP: parseFloat((item.Price * GBP).toFixed(2)),
        TRY: parseFloat((item.Price * TRY).toFixed(2)),
      }
    })
  })

  reservations.forEach(reservation => {

    const flight = capacities.find(f => f.ID === reservation.ReservationTypeID)
    flight.Capacity -= reservation.Tickets.length
    if (flight.Capacity < 0) flight.Capacity = 0

  })

  const result = {
    flights: capacities,
    currencies: {
      USD,
      GBP,
      TRY,
    },
    date: dateDDMMYYYY
  }

  const key = await CryptoJS.AES.encrypt(JSON.stringify({...result, valid: moment().add(1, 'hours').utc(false).unix()}), config.get('aes_key'));

  return API.success(res, {
    ...result,
    token: key.toString()
  })

});

router.post('/3d', async (req, res) => {

  var {token, total, card, currency, flight, passengers, coupon} = req.body;

  const {error} = Joi.object({
    token: Joi.string().required(),
    // total: Joi.number().integer().min(1).required(),
    total: Joi.number().positive().required(),
    card: Joi.object({
      name: Joi.string().required(),
      cardNumber: Joi.string().required().creditCard(),
      expiration: Joi.string().required().length(5).regex(/^\d{2}\/\d{2}$/),
      security: Joi.string().required().min(3).max(4).regex(/^\d{3,4}$/),
    }).required(),
    flight: Joi.string().required(),
    currency: Joi.string().required().valid('EUR', 'USD', 'TRY', 'GBP'),
    passengers: Joi.array().min(1).max(4).required().items(Joi.object({
      Name: Joi.string().min(3).max(75).required(),
      Email: Joi.string().email().required(),
      Phone: Joi.string().min(11).required().min(11).max(14).regex(/^\d+$/),
      Age: Joi.number().integer().min(6).max(999).required(),
      Nationality: Joi.string().required().valid(
        'Turkey',
        'Belgium',
        'Canada',
        'China',
        'Denmark',
        'France',
        'Germany',
        'Greece',
        'Italy',
        'Japan',
        'Netherlands',
        'Russia',
        'Swaziland',
        'Sweden',
        'Switzerland',
        'Ukraine',
        'United Kingdom',
        'United States',
        'Afghanistan',
        'Aland Islands',
        'Albania',
        'Algeria',
        'American Samoa',
        'Andorra',
        'Angola',
        'Anguilla',
        'Antarctica',
        'Antigua and Barbuda',
        'Argentina',
        'Armenia',
        'Aruba',
        'Australia',
        'Austria',
        'Azerbaijan',
        'Bahamas',
        'Bahrain',
        'Bangladesh',
        'Barbados',
        'Belarus',
        'Belize',
        'Benin',
        'Bermuda',
        'Bhutan',
        'Bolivia',
        'Bonaire, Sint Eustatius and Saba',
        'Bosnia and Herzegovina',
        'Botswana',
        'Bouvet Island',
        'Brazil',
        'British Indian Ocean Territory',
        'Brunei',
        'Bulgaria',
        'Burkina Faso',
        'Burundi',
        'Cambodia',
        'Cameroon',
        'Cape Verde',
        'Cayman Islands',
        'Central African Republic',
        'Chad',
        'Chile',
        'Christmas Island',
        'Cocos (Keeling) Islands',
        'Colombia',
        'Comoros',
        'Congo',
        'Cook Islands',
        'Costa Rica',
        'Ivory Coast',
        'Croatia',
        'Cuba',
        'Curacao',
        'Cyprus',
        'Czech Republic',
        'Democratic Republic of the Congo',
        'Djibouti',
        'Dominica',
        'Dominican Republic',
        'Ecuador',
        'Egypt',
        'El Salvador',
        'Equatorial Guinea',
        'Eritrea',
        'Estonia',
        'Ethiopia',
        'Falkland Islands (Malvinas)',
        'Faroe Islands',
        'Fiji',
        'Finland',
        'French Guiana',
        'French Polynesia',
        'French Southern Territories',
        'Gabon',
        'Gambia',
        'Georgia',
        'Ghana',
        'Gibraltar',
        'Greenland',
        'Grenada',
        'Guadaloupe',
        'Guam',
        'Guatemala',
        'Guernsey',
        'Guinea',
        'Guinea-Bissau',
        'Guyana',
        'Haiti',
        'Heard Island and McDonald Islands',
        'Honduras',
        'Hong Kong',
        'Hungary',
        'Iceland',
        'India',
        'Indonesia',
        'Iran',
        'Iraq',
        'Ireland',
        'Isle of Man',
        'Israel',
        'Jamaica',
        'Jersey',
        'Jordan',
        'Kazakhstan',
        'Kenya',
        'Kiribati',
        'Kosovo',
        'Kuwait',
        'Kyrgyzstan',
        'Laos',
        'Latvia',
        'Lebanon',
        'Lesotho',
        'Liberia',
        'Libya',
        'Liechtenstein',
        'Lithuania',
        'Luxembourg',
        'Macao',
        'Macedonia',
        'Madagascar',
        'Malawi',
        'Malaysia',
        'Maldives',
        'Mali',
        'Malta',
        'Marshall Islands',
        'Martinique',
        'Mauritania',
        'Mauritius',
        'Mayotte',
        'Mexico',
        'Micronesia',
        'Moldava',
        'Monaco',
        'Mongolia',
        'Montenegro',
        'Montserrat',
        'Morocco',
        'Mozambique',
        'Myanmar (Burma)',
        'Namibia',
        'Nauru',
        'Nepal',
        'New Caledonia',
        'New Zealand',
        'Nicaragua',
        'Niger',
        'Nigeria',
        'Niue',
        'Norfolk Island',
        'North Korea',
        'Northern Mariana Islands',
        'Norway',
        'Oman',
        'Pakistan',
        'Palau',
        'Palestine',
        'Panama',
        'Papua New Guinea',
        'Paraguay',
        'Peru',
        'Phillipines',
        'Pitcairn',
        'Poland',
        'Portugal',
        'Puerto Rico',
        'Qatar',
        'Reunion',
        'Romania',
        'Rwanda',
        'Saint Barthelemy',
        'Saint Helena',
        'Saint Kitts and Nevis',
        'Saint Lucia',
        'Saint Martin',
        'Saint Pierre and Miquelon',
        'Saint Vincent and the Grenadines',
        'Samoa',
        'San Marino',
        'Sao Tome and Principe',
        'Saudi Arabia',
        'Senegal',
        'Serbia',
        'Seychelles',
        'Sierra Leone',
        'Singapore',
        'Sint Maarten',
        'Slovakia',
        'Slovenia',
        'Solomon Islands',
        'Somalia',
        'South Africa',
        'South Georgia and the South Sandwich Islands',
        'South Korea',
        'South Sudan',
        'Spain',
        'Sri Lanka',
        'Sudan',
        'Suriname',
        'Svalbard and Jan Mayen',
        'Syria',
        'Taiwan',
        'Tajikistan',
        'Tanzania',
        'Thailand',
        'Timor-Leste (East Timor)',
        'Togo',
        'Tokelau',
        'Tonga',
        'Trinidad and Tobago',
        'Tunisia',
        'Turkmenistan',
        'Turks and Caicos Islands',
        'Tuvalu',
        'Uganda',
        'United Arab Emirates',
        'United States Minor Outlying Islands',
        'Uruguay',
        'Uzbekistan',
        'Vanuatu',
        'Vatican City',
        'Venezuela',
        'Vietnam',
        'Virgin Islands, British',
        'Virgin Islands, US',
        'Wallis and Futuna',
        'Western Sahara',
        'Yemen',
        'Zambia',
        'Zimbabwe',
      ),
      PassportNumber: Joi.string().max(100).empty('').allow(null),
      Hotel: Joi.string().max(100).empty('').allow(null),
    })),
  }).validate({token, total, card, currency, flight, passengers});

  if (error) {

    for (let i = 0; i < error.details.length; i++) {
      let err = error.details[i]
      if (err.path[0] === 'passengers') return API.error(res, 'Passenger(s) information is not valid or missing')
      if (err.path[0] === 'card') return API.error(res, 'Card information is not valid')
    }

    return API.error(res, 'Your request was declined. Your session may have expired')

  }

  var tokenData = null

  try {
    tokenData = JSON.parse(CryptoJS.AES.decrypt(token, config.get('aes_key')).toString(CryptoJS.enc.Utf8))
  } catch (error) {
  }

  if (!tokenData) return API.error(res, 'Your session have expired.')

  if (tokenData.valid - moment().utc(false).unix() < 0) return API.error(res, 'Your session have expired.')

  const flights = await Flight.find({IsDeleted: false}).lean()

  const getSelectedFlight = tokenData.flights.find(item => item.ID === flight)

  if (!getSelectedFlight) return API.error(res, 'There is no more space in your selected flight.')

  if (!flights.find(item => item._id.toString() === getSelectedFlight.ID)) return API.error(res, 'There is no more space in your selected flight.')

  const check = await Coupon.findOne({Code: coupon, IsUsed: false});

  console.log('chechk', check);
  console.log('orjinal', passengers.length * getSelectedFlight.Price[currency]);
  console.log('total gelen', total);
  console.log('benim hesapladigim', ((passengers.length * getSelectedFlight.Price[currency] * (check ? 0.9 : 1)).toFixed(2)));

  if (check) {
    if ((passengers.length * getSelectedFlight.Price[currency] * 0.9).toFixed(2) !== total) return API.error(res, 'Total amount is not valid!')
  } else {
    if (passengers.length * getSelectedFlight.Price[currency] !== total) return API.error(res, 'Total amount is not valid!')
  }

  const getReservations = await Reservation.find({CancelDate: null, ReservationDate: tokenData.date, ReservationTypeID: flight})
  // let getTicketCount = await Ticket.countDocuments({CancelAt: null, Date: tokenData.date})

  let totalSoldTicket = 0
  getReservations.forEach(r => {
    totalSoldTicket += r.Tickets.length
  })

  let cp = await Capacity.findOne({Date: tokenData.date, Online: {$ne: false}})
  let cpItem = cp.Capacities.find(item => item.ID == flight)

  let capacity = cpItem.Capacity

  const freeSpace = capacity > totalSoldTicket ? (capacity - totalSoldTicket) : 0

  if (freeSpace < passengers.length) return API.error(res, `There is only ${freeSpace} vacancy left`)

  var currencyCode = '978' // Eur
  if (currency === 'USD') currencyCode = '840'
  if (currency === 'GBP') currencyCode = '826'
  if (currency === 'TRY') currencyCode = '949'


  const successURL = config.get('callback')
  const failURL = config.get('callback')

  const chargePrice = Math.ceil(total * 100)
  // const chargePrice = 100
  // currencyCode = '949'

  const orderID = (Math.random().toString(36).substring(2, 11) + Math.random().toString(36).substring(2, 11)).toUpperCase()
  // HashData = terminalId + orderid + amount + currency + okurl + failurl + islemtipi + taksit + storekey + SecurityData
  const Hash3DEski = HEX.convert(bankTerminalID + orderID + chargePrice + currencyCode + successURL + failURL + 'sales' + '' + storeKey + hashedPassword)
  const Hash3D = (SHA.sha512(bankTerminalID + orderID + chargePrice + currencyCode + successURL + failURL + 'sales' + '' + storeKey + hashedPassword)).toUpperCase();

  console.log('eski', {Hash3DEski})
  console.log('yeni', {Hash3D})

  const cardSplit = card.expiration.split('/')
  const bankRequest = {
    mode: 'PROD',
    apiversion: '512',
    terminalprovuserid: 'PROVAUT',
    terminaluserid: 'PROVAUT',
    terminalmerchantid: '1073124',
    txntype: 'sales',
    txnamount: chargePrice,
    txncurrencycode: currencyCode,
    txninstallmentcount: '',
    orderid: orderID,
    terminalid: bankTerminalID,
    successurl: successURL,
    errorurl: failURL,
    customeripaddress: '127.0.0.1',
    secure3dhash: Hash3D,
    customeremailaddress: 'fly@butterflyballoons.com',
    secure3dsecuritylevel: '3D',
    cardnumber: card.cardNumber,
    cardexpiredatemonth: cardSplit[0],
    cardexpiredateyear: cardSplit[1],
    cardcvv2: card.security
  }

  const formInputs = []
  for (var bankItem in bankRequest) {
    formInputs.push(`<textarea style="display:none;" name="${bankItem}">${bankRequest[bankItem]}</textarea>`);
  }

  const order = await new Order({
    ID: orderID,
    Coupon: coupon || null,
    FreeSpace: freeSpace,
    TicketCount: passengers.length,
    Currency: currency,
    CurrencyCode: currencyCode,
    Total: chargePrice,
    Passengers: passengers,
    Flight: getSelectedFlight,
    Currencies: tokenData.currencies,
    OrderDate: moment().format('DD-MM-YYYY'),
    ReservationDate: tokenData.date,
    Card: card,
    CreatedAt: moment().unix(),
    UpdatedAt: moment().unix(),
    CanceledAt: null,

    CancelError: null,
    PurchaseError: null,

    Status: null,
    IsCanceled: false,
    OnProgress: false,

    FormRedirectRequest: bankRequest,
    PurchaseRequest: null,
    CancelRequest: null,

    RedirectionResponse: null,
    PurchaseResponse: null,
    CancelResponse: null,
    AlertStatus: false
  }).save()

  if (!order) return API.error(res, 'Something went wrong. We cannot continue, please try again')


  var html = `<form action="https://sanalposprov.garanti.com.tr/servlet/gt3dengine" method="post" name="payForm" id="payForm">${formInputs.join('')}</form>
<script type="text/javascript">document.payForm.submit();</script>`;

  return API.success(res, {
    form: html
  })

})

router.post('/charge', [bodyParser.urlencoded()], async (req, res) => {

    console.log('req.body geldi', req.body)

  const order = await Order.findOne({ID: req.body.orderid, Status: null, OnProgress: false}).lean()

  if (!order) return res.send(IframeResponse.error('Invalid order number, your session might be expired'))

  await Order.findOneAndUpdate({ID: order.ID}, {RedirectionResponse: req.body, OnProgress: true})

  const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  const customerEmailAddress = validateEmail(order.Passengers[0]['Email']) ? order.Passengers[0]['Email'] : 'fly@butterflyballoons.com'

  if (req.body.mdstatus === "1") {

    const Hash3DEski = HEX.convert(order.ID + bankTerminalID + order.Total + hashedPassword);
    const Hash3D = (SHA.sha512(order.ID + bankTerminalID + order.Total + order.CurrencyCode + hashedPassword)).toUpperCase();
  
    console.log('TAM DOGRULAMA SUPER!! ',{
        Hash3DEski,
        Hash3D
    })

    const PurchaseRequest = `<?xml version="1.0" encoding="UTF-8"?>
    <GVPSRequest>
      <Mode>PROD</Mode>
      <Version>512</Version>
      <ChannelCode></ChannelCode>
      <Terminal>
        <ProvUserID>PROVAUT</ProvUserID>
        <HashData>${Hash3D}</HashData>
        <UserID>PROVAUT</UserID>
        <ID>${bankTerminalID}</ID>
        <MerchantID>1073124</MerchantID>
      </Terminal>
      <Customer>
        <IPAddress>127.0.0.1</IPAddress>
        <EmailAddress>${customerEmailAddress}</EmailAddress>
      </Customer>
      <Order>
        <OrderID>${order.ID}</OrderID>
        <GroupID></GroupID>
        <Description></Description>
      </Order>
      <Transaction>
        <Type>sales</Type>
        <InstallmentCnt></InstallmentCnt>
        <Amount>${order.Total}</Amount>
        <CurrencyCode>${order.CurrencyCode}</CurrencyCode>
        <CardholderPresentCode>13</CardholderPresentCode>
        <MotoInd>N</MotoInd>
        <Secure3D>
          <AuthenticationCode>${encodeURI(req.body.cavv)}</AuthenticationCode>
          <SecurityLevel>${encodeURI(req.body.eci)}</SecurityLevel>
          <TxnID>${encodeURI(req.body.xid)}</TxnID>
          <Md>${encodeURI(req.body.md)}</Md>
        </Secure3D>
      </Transaction>
     </GVPSRequest>`

    return await request({
      method: 'POST',
      url: 'https://sanalposprov.garanti.com.tr/VPServlet',
      headers: {},
      body: PurchaseRequest
    }, async (error, response) => {

      if (error) {

        await Order.findOneAndUpdate({ID: order.ID}, {PurchaseRequest, PurchaseError: error, Status: false, OnProgress: false, UpdatedAt: moment().unix()})

        return res.send(IframeResponse.error('Order can not printed, please contact the sales support.'))

      } else {

        const PurchaseStatusCode = response.statusCode
        const PurchaseResponse = response.body

        let Status = false

        if (PurchaseStatusCode === 200 && PurchaseResponse.match(/\<Message\>Approved\<\/Message\>/)) Status = true

        await Order.findOneAndUpdate({ID: order.ID}, {PurchaseRequest, PurchaseResponse, Status, OnProgress: false, UpdatedAt: moment().unix()})

        if (Status) {

            if (order.Coupon) {
                await Coupon.findOneAndUpdate({Code: order.Coupon}, {
                    IsUsed: true,
                    DiscountAmount: parseFloat(((order.Total / 0.9) * 0.1).toFixed(2)),
                    DiscountCurrency: order.Currency,
                    Currencies: order.Rates,
                });
            }

          const flights = await Flight.find({IsDeleted: false}).lean()

          const flight = flights.find(item => item._id.toString() == order.Flight.ID)

          const getCurrencies = await Currency.findOne({}, null, {sort: {_id: -1}}).lean()

          const getCharges = []

          order.Passengers.forEach(item => {

            item.ID = uuidv4()
            item.IsNew = false
            item.Date = moment().format('DD.MM.YYYY')
            item.Type = 'BILET'
            item.Description = flight.Name
            item.Price = order.Total / order.Passengers.length
            item.Currency = order.Currency
            item.Currencies = getCurrencies.Rates
            item.PriceView = order.Total / order.Passengers.length / 100
            getCharges.push(item)

          })

          getCharges.push({
            ID: uuidv4(),
            IsNew: false,
            Date: moment().format('DD.MM.YYYY'),
            Type: 'POS_SANAL',
            Description: '3D Secure Online',
            Price: order.Total,
            Currency: order.Currency,
            Currencies: getCurrencies.Rates,
            PriceView: order.Total / 100,
          })

          const orderTickets = []

          order.Passengers.forEach(passenger => {

            orderTickets.push({
              BalloonID: null,
              ServiceID: null,
              Compartment: null,
              Name: passenger.Name,
              Age: passenger.Age,
              Email: passenger.Email,
              Gender: "",
              HotelName: passenger.Hotel,
              HotelRoom: "",
              Nation: passenger.Nationality || '',
              Passport: passenger.PassportNumber || '',
              Phone: passenger.Phone,
              ServiceTime: "",
              InformPickUp: false,
              InformCall: false,
              TID: uuidv4()
            })

          })

          const orderTransactions = []

          orderTickets.forEach(ticket => {

            orderTransactions.push({
              Description: order.Flight.Name,
              TicketID: ticket.TID,
              ReservationTypeID: order.Flight.ID,
              Date: moment().format('DD-MM-YYYY'),
              Type: "BILET",
              Price: order.Flight.Price[order.Currency] * 100,
              Currency: order.Currency,
              CurrencyDate: getCurrencies.Date,
              Currencies: {
                ...getCurrencies.Rates,
                EUR: 1,
                ...order.Currencies
              },
              ID: uuidv4()
            })

          })

          orderTransactions.push({
            Description: "Online 3D Satış",
            TicketID: null,
            ReservationTypeID: null,
            Date: moment().format('DD-MM-YYYY'),
            Type: "POS_SANAL",
            Price: order.Total,
            Currency: order.Currency,
            CurrencyDate: getCurrencies.Date,
            Currencies: {
              ...getCurrencies.Rates,
              EUR: 1,
              ...order.Currencies
            },
            ID: uuidv4(),
          })

          const sumDebt = order.Currency === 'EUR' ? order.Total : parseFloat( (order.Total / order.Currencies[order.Currency]).toFixed(2) )

          const OnlineID = await OnlineKey()

          const newReservation = await new Reservation({
            ID: uuidv4(),
            OnlineID,
            OrderID: order.ID,
            WaitingPayment: false,
            Pickup: true,
            Agency: {
              ID: null,
              Fixed: "",
              Rate: "",
              Single: true,
            },
            CreateDate: moment().format('DD-MM-YYYY'),
            ReservationDate: order.ReservationDate,
            ReservationTypeID: order.Flight.ID,
            Summary: {
              Chest: {
                Debt: 0,
                Credit: 0
              },
              Total: {
                Debt: sumDebt,
                Credit: sumDebt
              }
            },
            Tickets: orderTickets,
            Transactions: orderTransactions,
            Commission: 0,
            IsOnline: true,
            CreatedBy: {
              ID: null,
              Name: "Online Payment",
              IsAdmin: true,
              OrderID: order.ID
            },
            ClosedBy: null,
            CancelBy: null,
            CancelDate: null,
          }).save()

          const tickets = []

          let pdfContent = ''

          newReservation.Tickets.forEach(item => {

            const Price = order.Total / order.Passengers.length / 100
            const Flight = flight.Name
            const Currency = order.Currency

            tickets.push({
              Name: item.Name,
              Email: item.Email,
              Phone: item.Phone,
              Age: item.Age,
              TID: item.TID,
              Date: order.ReservationDate,
              Flight,
              Price,
              Currency,
            })

            pdfContent += `
<div class="ticket-item">
		<div>
			<div><label>Reservation ID</label> <span><b>${OnlineID}</b></span></div>
			<div><label>Date</label> <span>${order.ReservationDate}</span></div>
		</div>
		<div>
			<div><label>Flight</label> <span>${Flight}</span></div>
			<div><label>Price</label> <span>${Price} ${Currency} / <b style="color: #4f9632;">PAID</b></span></div>
		</div>
		<div>
			<div><label>Name</label> <span>${item.Name}</span></div>
			<div><label>Email</label> <span>${item.Email}</span></div>
		</div>
		<div>
			<div><label>Phone</label> <span>${item.Phone}</span></div>
			<div><label>Age</label> <span>${item.Age}</span></div>
		</div>
</div>`
          })

          await PDF.create(`${order.ID}.pdf`, pdfContent)

          await forEach(order.Passengers, async (passenger) => {

            await Mailer.send('Reservation Details', passenger.Email, passenger.Name, ['Thank you, we received your payment. Your reservation is ready, reservation details attached as pdf', 'P.S. If you need help to arrange hotel/ other tours / transfers please contact our sister company Turkish Heritage Travel ( www.goreme.com ) . If you e mail info@goreme.com they will be delighted to help you too.', 'We look forward to seeing you in the flight morning', 'If you have any further questions please don\'t hesitate to contact us.', 'Kind regards'], `${order.ID}.pdf`)

          })

          const alertRequest = {
            name: tickets[0].Name,
            date: order.ReservationDate,
            count: tickets.length,
            type: order.Flight.Name,
            price: `${parseFloat((order.Total / 100).toFixed(2))} ${order.Currency}`,
          }

          axios.post('https://panel.butterflyballoons.com/api/alert/reservation', alertRequest)
            .then(function (response) {
              Order.findOneAndUpdate({ID: order.ID}, {
                AlertStatus: true, AlertResponse: {
                  request: alertRequest,
                  response: response.data
                }
              })
            })
            .catch(function (error) {
              Order.findOneAndUpdate({ID: order.ID}, {AlertStatus: false, AlertResponse: {
                  request: alertRequest,
                  response: error
                }
              })
            })

          return res.send(IframeResponse.success('Please await..', tickets))


        }

        if (PurchaseResponse.match(/\<Code\>51\<\/Code\>/)) res.send(IframeResponse.error('Insufficient funds'))
        if (PurchaseResponse.match(/\<CardType\>YDAMEX\<\/CardType\>/)) res.send(IframeResponse.error('Sorry, we can only accept TL payments from AMEX cards.'))
        if (PurchaseResponse.match(/\<SysErrMsg\>Yurtici kartlar YP islem yapamaz\<\/SysErrMsg\>/)) res.send(IframeResponse.error('Only TL transactions can be made with Turkish cards.'))
        if (PurchaseResponse.match(/\<SysErrMsg\>Do not honor\<\/SysErrMsg\>/)) res.send(IframeResponse.error('Sorry, the credit card has been completely rejected by the issuing bank. Please contact your bank or try another card'))
        if (PurchaseResponse.match(/\<Code\>92\<\/Code\>/)) res.send(IframeResponse.error('Your card is not allowed to complete 3D transaction. Please try another card.'))

        return res.send(IframeResponse.error('Payment can not accepted, please contact the sales support.'))

      }

    });

  } else {

    // md status 1 değil yani tam doğrulama yapılamadı o yüzden normal çekim denenecek

    const cardExpiration = order.Card.expiration.split('/').join('');

    

    const hashDataEski = HEX.convert(order.ID + bankTerminalID + order.Card.cardNumber + order.Total + hashedPassword);
    const hashData = (SHA.sha512(order.ID + bankTerminalID + order.Card.cardNumber + order.Total + order.CurrencyCode + hashedPassword)).toUpperCase();

    console.log('MD status 1 degil! normal cekim denenecek', {
        hashDataEski, hashData
    })

    const PurchaseRequestNormal = `<?xml version="1.0" encoding="UTF-8"?>
    <GVPSRequest>
      <Mode>PROD</Mode>
      <Version>512</Version>
      <ChannelCode></ChannelCode>
      <Terminal>
        <ProvUserID>PROVAUT</ProvUserID>
        <HashData>${hashData}</HashData>
        <UserID>PROVAUT</UserID>
        <ID>${bankTerminalID}</ID>
        <MerchantID>1073124</MerchantID>
      </Terminal>
      <Customer>
        <IPAddress>127.0.0.1</IPAddress>
        <EmailAddress>${customerEmailAddress}</EmailAddress>
      </Customer>
      <Card>
        <Number>${order.Card.cardNumber}</Number>
        <ExpireDate>${cardExpiration}</ExpireDate>
        <CVV2>${order.Card.security}</CVV2>
      </Card>
      <Order>
        <OrderID>${order.ID}</OrderID>
        <GroupID></GroupID>
        <Description></Description>
      </Order>
      <Transaction>
        <Type>sales</Type>
        <InstallmentCnt></InstallmentCnt>
        <Amount>${order.Total}</Amount>
        <CurrencyCode>${order.CurrencyCode}</CurrencyCode>
        <CardholderPresentCode>0</CardholderPresentCode>
        <MotoInd>N</MotoInd>
      </Transaction>
     </GVPSRequest>`


    return await request({
      method: 'POST',
      url: 'https://sanalposprov.garanti.com.tr/VPServlet',
      headers: {},
      body: PurchaseRequestNormal
    }, async (error, response) => {

      if (error) {

        await Order.findOneAndUpdate({ID: order.ID}, {PurchaseRequest: PurchaseRequestNormal, PurchaseResponse: response.body, PurchaseError: error, Status: false, OnProgress: false, UpdatedAt: moment().unix()})

        return res.send(IframeResponse.error('Order can not process, please contact the sales support.'))

      } else {

        const PurchaseStatusCode = response.statusCode
        const PurchaseResponse = response.body

        let Status = false

        if (PurchaseStatusCode === 200 && PurchaseResponse.match(/\<Message\>Approved\<\/Message\>/)) Status = true

        await Order.findOneAndUpdate({ID: order.ID}, {PurchaseRequest: PurchaseRequestNormal, PurchaseResponse, Status, OnProgress: false, UpdatedAt: moment().unix()})

        if (Status) {

          const flights = await Flight.find({IsDeleted: false}).lean()

          const flight = flights.find(item => item._id.toString() == order.Flight.ID)

          const getCurrencies = await Currency.findOne({}, null, {sort: {_id: -1}}).lean()

          const getCharges = []

          order.Passengers.forEach(item => {

            item.ID = uuidv4()
            item.IsNew = false
            item.Date = moment().format('DD.MM.YYYY')
            item.Type = 'BILET'
            item.Description = flight.Name
            item.Price = order.Total / order.Passengers.length
            item.Currency = order.Currency
            item.Currencies = getCurrencies.Rates
            item.PriceView = order.Total / order.Passengers.length / 100
            getCharges.push(item)

          })

          getCharges.push({
            ID: uuidv4(),
            IsNew: false,
            Date: moment().format('DD.MM.YYYY'),
            Type: 'POS_SANAL',
            Description: 'Online Normal',
            Price: order.Total,
            Currency: order.Currency,
            Currencies: getCurrencies.Rates,
            PriceView: order.Total / 100,
          })

          const orderTickets = []

          order.Passengers.forEach(passenger => {

            orderTickets.push({
              BalloonID: null,
              ServiceID: null,
              Compartment: null,
              Name: passenger.Name,
              Age: passenger.Age,
              Email: passenger.Email,
              Gender: "",
              HotelName: passenger.Hotel,
              HotelRoom: "",
              Nation: "",
              Passport: "",
              Phone: passenger.Phone,
              ServiceTime: "",
              InformPickUp: false,
              InformCall: false,
              TID: uuidv4()
            })

          })

          const orderTransactions = []

          orderTickets.forEach(ticket => {

            orderTransactions.push({
              Description: order.Flight.Name,
              TicketID: ticket.TID,
              ReservationTypeID: order.Flight.ID,
              Date: moment().format('DD-MM-YYYY'),
              Type: "BILET",
              Price: order.Flight.Price[order.Currency] * 100,
              Currency: order.Currency,
              CurrencyDate: getCurrencies.Date,
              Currencies: {
                ...getCurrencies.Rates,
                EUR: 1,
                ...order.Currencies
              },
              ID: uuidv4()
            })

          })

          orderTransactions.push({
            Description: "Online 3D Satış",
            TicketID: null,
            ReservationTypeID: null,
            Date: moment().format('DD-MM-YYYY'),
            Type: "POS_SANAL",
            Price: order.Total,
            Currency: order.Currency,
            CurrencyDate: getCurrencies.Date,
            Currencies: {
              ...getCurrencies.Rates,
              EUR: 1,
              ...order.Currencies
            },
            ID: uuidv4(),
          })

          const OnlineID = await OnlineKey()

          const newReservation = await new Reservation({
            ID: uuidv4(),
            OnlineID,
            OrderID: order.ID,
            WaitingPayment: false,
            Pickup: true,
            Agency: {
              ID: null,
              Fixed: "",
              Rate: "",
              Single: true,
            },
            CreateDate: moment().format('DD-MM-YYYY'),
            ReservationDate: order.ReservationDate,
            ReservationTypeID: order.Flight.ID,
            Summary: {
              Chest: {
                Debt: 0,
                Credit: 0
              },
              Total: {
                Debt: order.Total,
                Credit: order.Total
              }
            },
            Tickets: orderTickets,
            Transactions: orderTransactions,
            Commission: 0,
            IsOnline: true,
            CreatedBy: {
              ID: null,
              Name: "Online Payment",
              IsAdmin: true,
              OrderID: order.ID
            },
            ClosedBy: null,
            CancelBy: null,
            CancelDate: null,
          }).save()

          const tickets = []

          let pdfContent = ''

          newReservation.Tickets.forEach(item => {

            const Price = order.Total / order.Passengers.length / 100
            const Flight = flight.Name
            const Currency = order.Currency

            tickets.push({
              Name: item.Name,
              Email: item.Email,
              Phone: item.Phone,
              Age: item.Age,
              TID: item.TID,
              Date: order.ReservationDate,
              Flight,
              Price,
              Currency,
            })

            pdfContent += `
<div class="ticket-item">
		<div>
			<div><label>Reservation ID</label> <span><b>${OnlineID}</b></span></div>
			<div><label>Date</label> <span>${order.ReservationDate}</span></div>
		</div>
		<div>
			<div><label>Flight</label> <span>${Flight}</span></div>
			<div><label>Price</label> <span>${Price} ${Currency} / <b style="color: #4f9632;">PAID</b></span></div>
		</div>
		<div>
			<div><label>Name</label> <span>${item.Name}</span></div>
			<div><label>Email</label> <span>${item.Email}</span></div>
		</div>
		<div>
			<div><label>Phone</label> <span>${item.Phone}</span></div>
			<div><label>Age</label> <span>${item.Age}</span></div>
		</div>
</div>`
          })

          await PDF.create(`${order.ID}.pdf`, pdfContent)

          await forEach(order.Passengers, async (passenger) => {

            await Mailer.send('Reservation Details', passenger.Email, passenger.Name, ['Your reservation is ready, details attached', 'Your reservation information is attached' , 'P.S. If you need help you arrange hotel/ other tours / transfers please contact our sister company Turkish Heritage Travel ( www.goreme.com ) . If you e mail info@goreme.com they will be delighted to help you too.', 'Thank you'], `${order.ID}.pdf`)

          })

          const alertRequest = {
            name: tickets[0].Name,
            date: order.ReservationDate,
            count: tickets.length,
            type: order.Flight.Name,
            price: `${parseFloat((order.Total / 100).toFixed(2))} ${order.Currency}`,
          }

          axios.post('https://panel.butterflyballoons.com/api/alert/reservation', alertRequest)
            .then(function (response) {
              Order.findOneAndUpdate({ID: order.ID}, {
                AlertStatus: true, AlertResponse: {
                  request: alertRequest,
                  response: response.data
                }
              })
            })
            .catch(function (error) {
              Order.findOneAndUpdate({ID: order.ID}, {AlertStatus: false, AlertResponse: {
                  request: alertRequest,
                  response: error
                }
              })
            })

          return res.send(IframeResponse.success('Please await..', tickets))


        }

        if (PurchaseResponse.match(/\<Code\>51\<\/Code\>/)) res.send(IframeResponse.error('Insufficient funds'))
        if (PurchaseResponse.match(/\<CardType\>YDAMEX\<\/CardType\>/)) res.send(IframeResponse.error('Sorry, we can only accept TL payments from AMEX cards.'))
        if (PurchaseResponse.match(/\<SysErrMsg\>Yurtici kartlar YP islem yapamaz\<\/SysErrMsg\>/)) res.send(IframeResponse.error('Only TL transactions can be made with Turkish cards.'))
        if (PurchaseResponse.match(/\<SysErrMsg\>Do not honor\<\/SysErrMsg\>/)) res.send(IframeResponse.error('Sorry, the credit card has been completely rejected by the issuing bank. Please contact your bank or try another card'))
        if (PurchaseResponse.match(/\<Code\>92\<\/Code\>/)) res.send(IframeResponse.error('Your card is not allowed to complete 3D transaction. Please try another card.'))

        return res.send(IframeResponse.error('Payment can not accepted, please contact the sales support.'))

      }

    })



    // if (PurchaseResponse.match(/\<Code\>92\<\/Code\>/)) res.send(IframeResponse.error('Your card is not allowed to complete 3D transaction. Please try another card.'))

    // md status 1 değilse
    // return res.send(IframeResponse.error('Payment can not accepted, please try again using a different credit card.'))

  }

})

router.post('/cancel-hide-for-now', async (req, res) => {

  const order = await Order.findOne({ID: req.body.id, Status: true, OnProgress: false, IsCanceled: false}).lean()

  if (!order) return API.error(res, 'Order not found')

  if (moment(order.ReservationDate, "YYYY-MM-DD").diff(moment().startOf('day'), 'days') <= 2) return API.error(res, 'The order can be canceled 48 hours before the flight')

  await Order.findOneAndUpdate({ID: order.ID}, {OnProgress: true})

  const CancelRequest = `<?xml version="1.0" encoding="UTF-8"?>
    <GVPSRequest>
      <Mode>PROD</Mode>
      <Version>v1.0</Version>
      <ChannelCode></ChannelCode>
      <Terminal>
        <ProvUserID>PROVRFN</ProvUserID>
        <HashData>${HEX.convert(order.ID + bankTerminalID + order.Total + hashedPassword)}</HashData>
        <UserID>PROVRFN</UserID>
        <ID>${bankTerminalID}</ID>
        <MerchantID>1073124</MerchantID>
      </Terminal>
      <Customer>
        <IPAddress>127.0.0.1</IPAddress>
        <EmailAddress>${order.Passengers[0]['Email']}</EmailAddress>
      </Customer>
      <Order>
        <OrderID>${order.ID}</OrderID>
        <GroupID></GroupID>
        <Description></Description>
      </Order>
      <Transaction>
        <Type>void</Type>
        <InstallmentCnt></InstallmentCnt>
        <Amount>${order.Total}</Amount>
        <CurrencyCode>${order.CurrencyCode}</CurrencyCode>
        <CardholderPresentCode>0</CardholderPresentCode>
        <MotoInd>N</MotoInd>
        <OriginalRetrefNum></OriginalRetrefNum>
      </Transaction>
     </GVPSRequest>`

  return await request({
    method: 'POST',
    url: 'https://sanalposprov.garanti.com.tr/VPServlet',
    headers: {},
    body: CancelRequest
  }, async (error, response) => {

    if (error) {

      await Order.findOneAndUpdate({ID: order.ID}, {CancelRequest, CancelError: error, OnProgress: false, UpdatedAt: moment().unix()})

      return API.error(res, 'Order can not cancel right now, please contact the sales support.')

    } else {

      const CancelStatusCode = response.statusCode
      const CancelResponse = response.body

      let CanceledAt = null
      let IsCanceled = false

      if (CancelStatusCode === 200 && CancelResponse.match(/\<Message\>Approved\<\/Message\>/)) {
        IsCanceled = true
        CanceledAt = moment().unix()
      }

      await Order.findOneAndUpdate({ID: order.ID}, {CancelRequest, CancelResponse, IsCanceled, CanceledAt, OnProgress: false, UpdatedAt: moment().unix()})

      if (IsCanceled) return API.success(res, {
        OrderID: order.ID,
        Operation: true
      })

      return API.error(res, 'Order can not cancel right now, please contact the sales support.')

    }
  });


})


module.exports = router;
